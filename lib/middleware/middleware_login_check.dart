import 'package:app_we_match/functions/token_lib.dart';
import 'package:app_we_match/pages/page_login.dart';
import 'package:app_we_match/pages/page_menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? token = await TokenLib.getToken();

    if (token == null || token == '') {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => PageMenu()), (route) => false);
    }
  }
}