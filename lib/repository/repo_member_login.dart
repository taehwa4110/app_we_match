import 'package:app_we_match/config/config_api.dart';
import 'package:app_we_match/model/member_login_request.dart';
import 'package:app_we_match/model/member_login_single_reuslt.dart';
import 'package:dio/dio.dart';

class RepoMemberLogin {
  Future<MemberLoginSingleResult> doLogin(MemberLoginRequest loginRequest) async {
    String baseUrl ='$apiUri/login/app/user';

    Dio dio = Dio();

    final response = await dio.post(
      baseUrl,
      data: loginRequest.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return MemberLoginSingleResult.fromJson(response.data);
  }
}