import 'package:app_we_match/components/component_custom_loading.dart';
import 'package:app_we_match/components/component_notification.dart';
import 'package:app_we_match/config/config_form_validator.dart';
import 'package:app_we_match/functions/token_lib.dart';
import 'package:app_we_match/middleware/middleware_login_check.dart';
import 'package:app_we_match/model/member_login_request.dart';
import 'package:app_we_match/repository/repo_member_login.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(MemberLoginRequest loginRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMemberLogin().doLogin(loginRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();

      TokenLib.setToken(res.data.token);

      MiddlewareLoginCheck().check(context);

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 30,),
          Image.asset('assets/employee.png', width: 250, height: 250,),
          SizedBox(height: 20,),
          Text('로그인', style: TextStyle(fontSize:30, fontWeight: FontWeight.bold ),),
          SizedBox(height: 30,),
          FormBuilder(
              key: _formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 30, right: 30, bottom: 30),
                    child: FormBuilderTextField(
                      name: 'username',
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.account_circle),
                          filled: true,
                          labelText: '아이디'
                      ),
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(errorText: formErrorRequired),
                        FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                        FormBuilderValidators.maxLength(30, errorText: formErrorMaxLength(30)),
                      ]),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30, right: 30),
                    child: FormBuilderTextField(
                      name: 'password',
                      decoration: const InputDecoration(
                        prefixIcon: Icon(Icons.key),
                        filled: true,
                        labelText: '비밀번호',
                      ),
                      obscureText: true,
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(errorText: formErrorRequired),
                        FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                        FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                      ]),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  SizedBox(height: 50,),
                  Container(
                    height: 50,
                    width: 130,
                    child: OutlinedButton(
                        onPressed: () {
                          if (_formKey.currentState?.saveAndValidate() ?? false) {
                            MemberLoginRequest loginRequest = MemberLoginRequest(
                              _formKey.currentState!.fields['username']!.value,
                              _formKey.currentState!.fields['password']!.value,
                            );
                            _doLogin(loginRequest);
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('로그인', style: TextStyle(letterSpacing: 7, color: Colors.black), ),
                            SizedBox(width: 5,),
                            Icon(Icons.card_travel),
                          ],
                        )

                    ),
                  )
                ],
              )
          )

        ],
      ) ,
    );

  }
}