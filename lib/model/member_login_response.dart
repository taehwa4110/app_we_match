class MemberLoginResponse {
  String token;
  String name;

  MemberLoginResponse(this.token, this.name);

  factory MemberLoginResponse.fromJson(Map<String, dynamic> json) {
    return MemberLoginResponse(
      json['token'],
      json['name']
    );
  }
}