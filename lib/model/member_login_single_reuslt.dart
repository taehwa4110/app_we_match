import 'package:app_we_match/model/member_login_response.dart';

class MemberLoginSingleResult {
  bool isSuccess;
  int code;
  String msg;
  MemberLoginResponse data;

  MemberLoginSingleResult(this.isSuccess, this.code, this.msg, this.data);

  factory MemberLoginSingleResult.fromJson(Map<String, dynamic> json) {
    return MemberLoginSingleResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      MemberLoginResponse.fromJson(json['data'])
    );
  }
}